.. _swh-web:

.. toctree::
   :maxdepth: 3
   :caption: README:

   README

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   developers-info
   uri-scheme-api
   uri-scheme-browse
   uri-scheme-swhids
   uri-scheme-misc


.. only:: standalone_package_doc

   Indices and tables
   ------------------

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`routingtable`
   * :ref:`search`
